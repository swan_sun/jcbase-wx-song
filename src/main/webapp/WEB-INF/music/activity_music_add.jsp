<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ include file="/WEB-INF/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properities -->
  <title>Login Example - Semantic</title>
  <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/bootstrap.css" />
  <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/font-awesome.css" />
  <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/select2.css" />
  <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace-fonts.css" />
  <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace.css" />
  <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace-skins.css" />
  <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace-rtl.css" />
  <!--[if lte IE 9]>
   <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace-part2.css" class="ace-main-stylesheet" />
  <![endif]-->

  <!--[if lte IE 9]>
   <link rel="stylesheet" href="/res/ace-1.3.3/assets/css/ace-ie.css" />
  <![endif]-->
  <link rel="stylesheet" href="/res/js/omui/development-bundle/themes/default/om-fileupload.css" />
  <script src="/res/ace-1.3.3/assets/js/jquery.js" ></script>
  <script src="/res/ace-1.3.3/assets/js/ace-extra.js"></script>
  <script src="/res/js/semantic/assets/library/jquery.min.js"></script>
  <script src="/res/js/semantic/semantic.min.js"></script>
  <link rel="stylesheet" type="text/css" href="/uploadify/uploadify.css">
   <script src="/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
  <link rel="stylesheet" type="text/css" href="/res/js/semantic/semantic.min.css">
  <style type="text/css">
  	.uploadify-button{
  		background-color: white;
  	}
  	.uploadify:hover .uploadify-button{
		background-color: white;
  	}
  </style>
</head>

<body class="no-skin">
		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<div class="main-content" style="margin-left: 0px;">
					<div class="page-content">
						
						<div class="row">
						<div class="col-xs-12">
							<!-- PAGE CONTENT BEGINS -->
							<div class="widget-box">
								
								<div class="widget-body">
									<div class="widget-main">
										<!-- #section:plugins/fuelux.wizard.container -->
										<div class="step-content pos-rel" id="step-container">
											<div class="step-pane active" id="step1">
												<form class="form-horizontal" id="validation-form" method="post">
												
												<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="name">歌曲名:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="text" name="name" id="name" class="col-xs-12 col-sm-6" value="${item.name}"/>
															</div>
														</div>
													</div>
														<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="singer">歌手:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<input type="text" name="singer" id="singer" class="col-xs-12 col-sm-6" value="${item.singer}"/>
															</div>
														</div>
													</div>
													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="language">歌曲语言:</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="language" id="language" class="span3">
																<option value="国语" ${item.language eq '国语'?'selected':''}>国语</option>
																<option value="粤语" ${item.language eq '粤语'?'selected':''}>粤语</option>
																<option value="英文" ${item.language eq '英文'?'selected':''}>英文</option>
															</select>								
															</div>
														</div>
													</div>
													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="file_upload">上传歌曲:</label>

														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<div class="cover-area" style="border: 1px solid #e0e0e0;width: 80%;border-radius:5px;padding: 5px 0 0 5px;">
																	<div class="cover-hd">
																		<input id="file_upload" name="file_upload" type="file" />
																		<input id="music_url" class="cover-input" value="${item.music_url}" name="music_url" type="hidden" />
																	</div>
																	<p id="upload-tip" class="upload-tip"></p>
																	<p id="apkArea" class="cover-bd" style="display: ${action eq 'add'?'none':''}">
																			<a class="vb cover-del" href="#" style="width: 600px;">${item.music_url}</a>
																	</p>
																</div>
															</div>
														</div>
													</div>
													<div class="space-2"></div>
													<div class="form-group">
														<label class="control-label col-xs-12 col-sm-3 no-padding-right" for="os">难度级别:</label>
														<div class="col-xs-12 col-sm-9">
															<div class="clearfix">
																<select name="hard_level" id="hard_level" class="span3">
																<option value="1" ${item.hard_level eq 1?'selected':''}>简单</option>
																<option value="2" ${item.hard_level eq 2?'selected':''}>一般</option>
																<option value="3" ${item.hard_level eq 3?'selected':''}>困难</option>
															</select>								
															</div>
														</div>
													</div>
													<div class="space-2"></div>
													
													<div class="clearfix form-actions">
														<div class="col-md-offset-3 col-md-9">
															<button id="submit-btn" class="btn btn-info" type="submit" data-last="Finish">
																<i class="ace-icon fa fa-check bigger-110"></i>
																提交
															</button>
					
															&nbsp; &nbsp; &nbsp;
															<button class="btn" type="reset">
																<i class="ace-icon fa fa-undo bigger-110"></i>
																重置
															</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div><!-- /.widget-main -->
								</div><!-- /.widget-body -->
							</div>
						</div><!-- /.col -->
					</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
			</div><!-- /.main-container-inner -->
		</div><!-- /.main-container -->
		
		<script src="/res/ace-1.3.3/assets/js/bootstrap.js"></script>
		<script src="/res/ace-1.3.3/assets/js/jquery.validate.js"></script>
		<script src="/res/ace-1.3.3/assets/js/additional-methods.js"></script>
		<script src="/res/ace-1.3.3/assets/js/bootbox.js"></script>
		<script src="/res/ace-1.3.3/assets/js/jquery.maskedinput.js"></script>
		<script src="/res/ace-1.3.3/assets/js/select2.js"></script>
		<!-- ace scripts -->
		<script src="/res/ace-1.3.3/assets/js/ace-elements.js"></script>
		<script src="/res/ace-1.3.3/assets/js/ace.js"></script>

		<!-- inline scripts related to this page -->
		<script src="/res/js/layer/layer.js"></script>
		<script src="/res/datepicker/WdatePicker.js"></script>
    
	<script type="text/javascript">
		
			jQuery(function($) {
				
				$('#file_upload').uploadify({
					//校验数据
					'swf' : '../../uploadify/uploadify.swf', //指定上传控件的主体文件，默认‘uploader.swf’
					'uploader' : '/music/uploadMusic', //指定服务器端上传处理文件，默认‘upload.php’
					'auto' : true, //手动上传
					'buttonImage' : '../../uploadify/uploadify-upload.png', //浏览按钮背景图片
					'width' :110,
					'height' :30,
					'dataType':'json',
					'cancelImg': 'uploadify/uploadify-cancel.png',
					//'buttonText': '选 择应用',
					'multi' : false, //单文件上传
					'fileTypeExts' : '*.mp3', //允许上传的文件后缀
					'fileSizeLimit' : '1MB', //上传文件的大小限制，单位为B, KB, MB, 或 GB
					'successTimeout' : 30, //成功等待时间
					'onUploadSuccess' : function(file, data,response) {//每成功完成一次文件上传时触发一次
						 var json_data=eval("("+data+")");
						$("#apkArea").show().find(".cover-del").html(json_data.musicUrl);
						$("#music_url").val(json_data.musicUrl);
						$("#name").val(json_data.name);
					},
					'onUploadError' : function(file, data, response) {//当上传返回错误时触发
						$('#f_pics').append("<div class=\"pics_con\">" + data + "</div>");
					}
				});
				
				var $validation = true;
				
				$('#validation-form').validate({
					errorElement: 'div',
					errorClass: 'help-block',
					focusInvalid: false,
					//title versionNo url natureNo  contents
					rules: {
						name:{
							required: true
						},
						singer:{
							required: true
						},
						language:{
							required: true
						},
						music_url:{
							required: true
						},
						hard_level:{
							required: true
						}
						
					},
					messages: {
						name:{
							required: "歌曲名不能为空"
						},
						singer:{
							required: "歌手不能为空",
						},
						natureNo:{
							required: "自然版本号不能为空",
							number:"自然版本号必须为整数"
						},
						music_url:{
							required: "请上传歌曲文件"
						},
						hard_level:{
							required: "难度必选"
						}
					},
					highlight: function (e) {
						$(e).closest('.form-group').removeClass('has-info').addClass('has-error');
					},
			
					success: function (e) {
						$(e).closest('.form-group').removeClass('has-error');//.addClass('has-info');
						$(e).remove();
					},
			
					errorPlacement: function (error, element) {
						if(element.is(':checkbox') || element.is(':radio')) {
							var controls = element.closest('div[class*="col-"]');
							if(controls.find(':checkbox,:radio').length > 1) controls.append(error);
							else error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
						}
						else if(element.is('.select2')) {
							error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
						}
						else if(element.is('.chosen-select')) {
							error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
						}
						else error.insertAfter(element.parent());
					},
			
					submitHandler: function (form) {
						var $form = $("#validation-form");
						var $btn = $("#submit-btn");
						
						if($btn.hasClass("disabled")) return;
			
						var submitData = {
								id:"${item.id}",
								name:$("#name").val(),
								singer: $("#singer").val(),
								language:$("#language").val(),
								music_url:$("#music_url").val(),
								hard_level:$("#hard_level").val()
						};
						$btn.addClass("disabled");
						$.post('/music/save', submitData,function(data) {
							$btn.removeClass("disabled");
							if(data.code==0){
	        					window.parent.reloadGrid(); //重新载入
	        					layer.msg("保存成功", {
	        					    icon: 1,
	        					    time: 2000 //2秒关闭（如果不配置，默认是3秒）
	        					},function(){
	        						var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
	        						parent.layer.close(index); //再执行关闭 
	        					});
	        				}else{
	        					layer.msg(data.msg, {
	        					    icon: 2,
	        					    time: 2000 //2秒关闭（如果不配置，默认是3秒）
	        					},function(){
	        					});
	        				}
						},"json");
						return false;
					},
					invalidHandler: function (form) {
					}
				});
			
			});
			
			function closeView(){
				var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
				parent.layer.close(index); //再执行关闭 
			}
			
		</script>
</body>

</html>

